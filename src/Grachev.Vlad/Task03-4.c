#include <stdio.h>
#include <string.h>
#include <time.h>
#define N 10

int flStrArr(char strarr[N][N])
{
	int i = 1;
	fgets(strarr[0], N, stdin);
	strarr[0][strlen(strarr[0]) - 1] = 0;
	if (strarr[0][0] == 0)
		return 0;
	while(i < N)
	{
		fgets(strarr[i], N, stdin);	
		strarr[i][strlen(strarr[i]) - 1] = 0;
		if(strarr[i][0] == 0)
			i += N;
		else
			i++;
	}
	if(i > N)
		i -= N;
	return i;
}

int main()
{
	char strarr[N][N];
	int flags[N];
	int numlin, j, k;
	numlin = flStrArr(strarr);
	srand(time(0));
	for (j = 0; j < numlin; j++ )
		flags[j] = 0;
	k = 0;
	while(k < numlin)
	{
		j = rand()%numlin;
		if(flags[j] != 1)
		{
			flags[j] = 1;
			printf("%s\n", strarr[j]);
			k++;
		}
	}	
	return 0;
}	
