#include <stdio.h>
#include <string.h>
#define N 10

int flStrArr(char strarr[N][N])
{
	int i = 1;
	fgets(strarr[0], N, stdin);
	strarr[0][strlen(strarr[0]) - 1] = 0;
	if (strarr[0][0] == 0)
		return 0;
	while(i < N)
	{
		fgets(strarr[i], N, stdin);	
		strarr[i][strlen(strarr[i]) - 1] = 0;
		if(strarr[i][0] == 0)
			i += N;
		else
			i++;
	}
	if(i > N)
		i -= N;
	return i;
}

int sortindx(char strlngth[], char strindx[], int arrlng)
{
	char buf1, buf2;
	int i, j;
	for(i = 1; i < arrlng; i++)
	{
		buf1 = strlngth[i];
		buf2 = strindx[i];
		j = i - 1;
		while((j >= 0)&&(strlngth[j] > buf1))
		{
			strlngth[j+1] = strlngth[j];
			strindx[j+1] = strindx[j];
			j--;
		}
		strlngth[j+1] = buf1;
		strindx[j+1] = buf2;
		
	}
	return 0;
}

int main()
{
	char strarr[N][N];
	char strlngth[N];
	char strindx[N];
	int numlin,i;

	numlin = flStrArr(strarr);
	for(i = 0; i < numlin; i++)
	{
		strlngth[i] = strlen(strarr[i]);
		strindx[i] = i;
	}
	if(numlin > 0)
		i = sortindx(strlngth, strindx, numlin);
	for(i = 0; i < numlin; i++)
		printf("%s \n", strarr[strindx[i]]);
	return 0;
}