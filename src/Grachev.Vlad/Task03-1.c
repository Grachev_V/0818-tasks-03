#include <stdio.h>
#include <string.h>
#define N 10

int flStrArr(char strarr[N][N])
{
	int i = 1;
	fgets(strarr[0], N, stdin);
	strarr[0][strlen(strarr[0]) - 1] = 0;
	if (strarr[0][0] == 0)
		return 0;
	while(i < N)
	{
		fgets(strarr[i], N, stdin);	
		strarr[i][strlen(strarr[i]) - 1] = 0;
		if(strarr[i][0] == 0)
			i += N;
		else
			i++;
	}
	if(i > N)
		i -= N;
	return i;
}

int main()
{
	char strarr[N][N];
	int numlin;
	numlin = flStrArr(strarr);
	numlin--;
	while(numlin >= 0)
	{
		printf("%s \n", strarr[numlin]);
		numlin--;
	}
	return 0;
}	

